<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=1024" />
  <title>Individual variation, network heterogeneity and linguistic complexity: which way does the relationship go? | Sébastien Lerique @wehlutyk</title>
  <meta name="description" content="Contributed talk to IELC 2019 by Dan Dediu, Márton Karsai, Sébastien Lerique & Jean-Philippe Magué." />
  <meta name="author" content="Sébastien Lerique" />

  <link href="css/style.css" rel="stylesheet" />
</head>

<body class="impress-not-supported">

<div class="fallback-message">
  <p>Your browser <b>doesn't support the features required</b> by impress.js, so you are presented with a simplified version of this presentation.</p>
  <p>For the best experience please use a modern up-to-date browser.</p>
</div>

<div id="impress">

  <div id="start" class="step" data-x="-512" data-y="0">
    <h2>Individual variation, network heterogeneity and linguistic complexity: <br/> which way does the relationship go?</h2>
    <h3>Sébastien Lerique, Dan Dediu, Márton Karsai, <br /> & Jean-Philippe Magué</h3>
  </div>

  <div id="problem" class="step" data-rel-x="1024">
    <h1>The problem</h1>
    <div class="flex-h flex-align-center flex-justify-even">
      <p>Language must adapt to many</p>
      <ul class="left-line">
        <li>≠ landscapes</li>
        <li>≠ situations</li>
        <li>≠ sub-communities</li>
        <li>≠ material conditions</li>
        <li>≠ idiosyncracies</li>
      </ul>
    </div>
    <p>... in a complex network</p>
  </div>

  <div id="lowest-common" class="step" data-x="-1536" data-rel-y="1024">
    <h2 class="supertitle">Possible mechanisms</h2>
    <h1>Lowest common denominator</h1>
    <ul class="space-top space-inner-v">
      <li>“Train window” communication <span class="effect">rudimentary signs</span></li>
      <li>Advertisements <span class="effect">pre-filtered interpretations</span></li>
      <li>Administrative language <span class="effect">“syntax of abridgement”</span> <a class="cite" href="https://en.wikipedia.org/wiki/One-Dimensional_Man">Marcuse 1964</a></li>
      <li>Portable partial acts <a href="https://mitpress.mit.edu/books/linguistic-bodies" class="cite">Di Paolo <em>et al.</em> 2018</a></li>
      <li>
    </ul>
  </div>

  <div id="super-redundant" class="step" data-rel-x="1024" data-rel-y="0">
    <h2 class="supertitle">Possible mechanisms</h2>
    <h1>
      Super redundant-robust-complex
    </h1>
    <div class="flex-h flex-align-center flex-justify-even">
      <ul class="align-right right-line">
        <li>Repairing breakdowns</li>
        <li>Articulating something</li>
        <li>Meaning something</li>
        <li>Negotiating turns & interaction</li>
      </ul>
      <p>can each be done in many ways</p>
    </div>
    +
    <p>Redundancy turns into complexity when not needed</p>
  </div>

  <div id="hypothesis" class="step">
    <h1>Hypothesis</h1>
    <ul class="left-line center">
      <li>Standing variation in all aspects of language <span class="effect">fuel for redundancy</span></li>
      <li>Network heterogeneity <span class="effect">pressure for adaptation</span></li>
      <li>Interactional disorder <a href="https://mitpress.mit.edu/books/linguistic-bodies" class="cite">Di Paolo <em>et al.</em> 2018</a></li>
    </ul>
    <p>Are core factors generating redundancy, <br /> robustness & complexity</p>
  </div>

  <div id="examples" class="step">
    <h1>Examples</h1>
    <div class="flex-h flex-align-center flex-justify-even">
      <img width="500px" src="img/moisik-dediu-2017.png" />
      <p class="align-right">Vocal tract anatomy and articulation <a class="cite" href="https://academic.oup.com/jole/article/2/1/37/3831320">Moisik & Dediu 2017</a></p>
    </div>
    <div class="flex-h flex-align-center flex-justify-even">
      <p class="align-left">Chronic <em>otitis media</em> in Australian Aboriginal populations <a class="cite" href="https://www.taylorfrancis.com/books/9780203782989/chapters/10.4324/9780203782989-21">Butcher 2013</a></p>
      <img width="400px" src="img/australia-languages-map.png" />
    </div>
    <p class="align-left">More diversity than expected <a class="cite" href="http://journals.cambridge.org/article_S0140525X0999094X">Evans & Levinson 2009</a></p>
    <p class="align-left">Japanese business-card exchange rules <a class="cite" href="http://link.springer.com/article/10.1007/s11097-014-9404-9/fulltext.html">Cuffari <em>et al.</em> 2015</a></p>
  </div>

  <div id="genetics" class="step" data-x="-1024" data-rel-y="1024">
    <h2 class="supertitle">Inspirations</h2>
    <h1>Evolutionary biology</h1>
    <ul class="left-line center">
      <li>Niche construction <span class="effect">speakers construct their environment</span></li>
      <li>Developmental plasticity <span class="effect">learners adapt to their context</span></li>
      <li>Evolution of development <span class="effect">more constraints and opportunities</span></li>
    </ul>
    <p>Adaptation to different environments is dealt with dynamic, <br /> recursive and complex processes</p>
  </div>

  <div id="granovetter" class="step" data-rel-x="1024" data-rel-y="0">
    <h2 class="supertitle">Inspirations</h2>
    <h1>Population structure & variation</h1>

    <p>Minor changes in a population can have macroscopic effects <a class="cite" href="http://www.journals.uchicago.edu/doi/abs/10.1086/226707">Granovetter 1978</a> <a class="cite" href="http://www.journals.uchicago.edu/doi/abs/10.1086/261849">Bikhchandani <em>et al.</em> 1992</a></p>
  </div>

  <div id="granovetter-1" class="step" data-rel-x="0" data-rel-y="0">
    <div class="frame">
      <svg class="granovetter frame-0" viewBox="0 0 95.849989 56.162505" version="1.1">
        <g transform="translate(-23.134525,-86.167262)">
          <circle class="dot-0" cx="31.372025" cy="94.404762" r="7.9375" />
          <circle class="dot-1" cx="51.215775" cy="94.404762" r="7.9375" />
          <circle class="dot-2" cx="71.059525" cy="94.404762" r="7.9375" />
          <circle class="dot-3" cx="90.903275" cy="94.404762" r="7.9375" />
          <circle class="dot-4" cx="110.74702" cy="94.404762" r="7.9375" />
          <circle class="dot-5" cx="31.372025" cy="114.24851" r="7.9375" />
          <circle class="dot-6" cx="51.215775" cy="114.24851" r="7.9375" />
          <circle class="dot-7" cx="71.059525" cy="114.24851" r="7.9375" />
          <circle class="dot-8" cx="90.903275" cy="114.24851" r="7.9375" />
          <circle class="dot-9" cx="110.74702" cy="114.24851" r="7.9375" />
          <circle class="dot-10" cx="31.372025" cy="134.09227" r="7.9375" />
          <circle class="dot-11" cx="51.215775" cy="134.09227" r="7.9375" />
          <circle class="dot-12" cx="71.059525" cy="134.09227" r="7.9375" />
          <circle class="dot-13" cx="90.903275" cy="134.09227" r="7.9375" />
          <circle class="dot-14" cx="110.74702" cy="134.09227" r="7.9375" />
        </g>
        <g transform="translate(-23.134525,-86.167262)">
          <text x="31.377192" y="97.486748" class="number-0"><tspan x="31.377192" y="97.486748">0</tspan></text>
          <text x="51.12534" y="97.490875" class="number-1"><tspan x="51.12534" y="97.490875">1</tspan></text>
          <text x="71.204216" y="97.546692" class="number-2"><tspan x="71.204216" y="97.546692">2</tspan></text>
          <text x="90.926529" y="97.486748" class="number-3"><tspan x="90.926529" y="97.486748">3</tspan></text>
          <text x="110.78836" y="97.490875" class="number-4"><tspan x="110.78836" y="97.490875">4</tspan></text>
          <text x="31.428869" y="117.27468" class="number-5"><tspan x="31.428869" y="117.27468">5</tspan></text>
          <text x="51.182186" y="117.3305" class="number-6"><tspan x="51.182186" y="117.3305">6</tspan></text>
          <text x="71.080193" y="117.33463" class="number-7"><tspan x="71.080193" y="117.33463">7</tspan></text>
          <text x="90.90844" y="117.3305" class="number-8"><tspan x="90.90844" y="117.3305">8</tspan></text>
          <text x="110.78836" y="117.3305" class="number-9"><tspan x="110.78836" y="117.3305">9</tspan></text>
          <text x="31.144648" y="137.17426" class="number-10"><tspan x="31.144648" y="137.17426">10</tspan></text>
          <text x="51.12534" y="137.17839" class="number-11"><tspan x="51.12534" y="137.17839">11</tspan></text>
          <text x="71.010429" y="137.23419" class="number-12"><tspan x="71.010429" y="137.23419">12</tspan></text>
          <text x="90.748245" y="137.17426" class="number-13"><tspan x="90.748245" y="137.17426">13</tspan></text>
          <text x="110.46538" y="137.17839" class="number-14"><tspan x="110.46538" y="137.17839">14</tspan></text>
        </g>
      </svg>
    </div>
  </div>

  <div id="granovetter-2" class="step" data-rel-x="0" data-rel-y="0">
    <div class="frame flex-h flex-align-center">
      <span class="effect"></span>
      <svg class="granovetter granovetter-animated" viewBox="0 0 95.849989 56.162505" version="1.1">
        <g transform="translate(-23.134525,-86.167262)">
          <circle class="dot-0" cx="31.372025" cy="94.404762" r="7.9375" />
          <circle class="dot-1" cx="51.215775" cy="94.404762" r="7.9375" />
          <circle class="dot-2" cx="71.059525" cy="94.404762" r="7.9375" />
          <circle class="dot-3" cx="90.903275" cy="94.404762" r="7.9375" />
          <circle class="dot-4" cx="110.74702" cy="94.404762" r="7.9375" />
          <circle class="dot-5" cx="31.372025" cy="114.24851" r="7.9375" />
          <circle class="dot-6" cx="51.215775" cy="114.24851" r="7.9375" />
          <circle class="dot-7" cx="71.059525" cy="114.24851" r="7.9375" />
          <circle class="dot-8" cx="90.903275" cy="114.24851" r="7.9375" />
          <circle class="dot-9" cx="110.74702" cy="114.24851" r="7.9375" />
          <circle class="dot-10" cx="31.372025" cy="134.09227" r="7.9375" />
          <circle class="dot-11" cx="51.215775" cy="134.09227" r="7.9375" />
          <circle class="dot-12" cx="71.059525" cy="134.09227" r="7.9375" />
          <circle class="dot-13" cx="90.903275" cy="134.09227" r="7.9375" />
          <circle class="dot-14" cx="110.74702" cy="134.09227" r="7.9375" />
        </g>
        <g transform="translate(-23.134525,-86.167262)">
          <text x="31.377192" y="97.486748" class="number-0"><tspan x="31.377192" y="97.486748">0</tspan></text>
          <text x="51.12534" y="97.490875" class="number-1"><tspan x="51.12534" y="97.490875">1</tspan></text>
          <text x="71.204216" y="97.546692" class="number-2"><tspan x="71.204216" y="97.546692">2</tspan></text>
          <text x="90.926529" y="97.486748" class="number-3"><tspan x="90.926529" y="97.486748">3</tspan></text>
          <text x="110.78836" y="97.490875" class="number-4"><tspan x="110.78836" y="97.490875">4</tspan></text>
          <text x="31.428869" y="117.27468" class="number-5"><tspan x="31.428869" y="117.27468">5</tspan></text>
          <text x="51.182186" y="117.3305" class="number-6"><tspan x="51.182186" y="117.3305">6</tspan></text>
          <text x="71.080193" y="117.33463" class="number-7"><tspan x="71.080193" y="117.33463">7</tspan></text>
          <text x="90.90844" y="117.3305" class="number-8"><tspan x="90.90844" y="117.3305">8</tspan></text>
          <text x="110.78836" y="117.3305" class="number-9"><tspan x="110.78836" y="117.3305">9</tspan></text>
          <text x="31.144648" y="137.17426" class="number-10"><tspan x="31.144648" y="137.17426">10</tspan></text>
          <text x="51.12534" y="137.17839" class="number-11"><tspan x="51.12534" y="137.17839">11</tspan></text>
          <text x="71.010429" y="137.23419" class="number-12"><tspan x="71.010429" y="137.23419">12</tspan></text>
          <text x="90.748245" y="137.17426" class="number-13"><tspan x="90.748245" y="137.17426">13</tspan></text>
          <text x="110.46538" y="137.17839" class="number-14"><tspan x="110.46538" y="137.17839">14</tspan></text>
        </g>
      </svg>
    </div>
  </div>

  <div id="granovetter-3" class="step" data-rel-x="0" data-rel-y="0">
    <div class="frame">
      <svg class="granovetter" viewBox="0 0 95.849989 56.162505" version="1.1">
        <g transform="translate(-23.134525,-86.167262)">
          <circle class="dot-0" cx="31.372025" cy="94.404762" r="7.9375" />
          <circle class="dot-1" cx="51.215775" cy="94.404762" r="7.9375" />
          <circle class="dot-2" cx="71.059525" cy="94.404762" r="7.9375" />
          <circle class="dot-cut" cx="90.903275" cy="94.404762" r="7.9375" />
          <circle class="dot-4" cx="110.74702" cy="94.404762" r="7.9375" />
          <circle class="dot-5" cx="31.372025" cy="114.24851" r="7.9375" />
          <circle class="dot-6" cx="51.215775" cy="114.24851" r="7.9375" />
          <circle class="dot-7" cx="71.059525" cy="114.24851" r="7.9375" />
          <circle class="dot-8" cx="90.903275" cy="114.24851" r="7.9375" />
          <circle class="dot-9" cx="110.74702" cy="114.24851" r="7.9375" />
          <circle class="dot-10" cx="31.372025" cy="134.09227" r="7.9375" />
          <circle class="dot-11" cx="51.215775" cy="134.09227" r="7.9375" />
          <circle class="dot-12" cx="71.059525" cy="134.09227" r="7.9375" />
          <circle class="dot-13" cx="90.903275" cy="134.09227" r="7.9375" />
          <circle class="dot-14" cx="110.74702" cy="134.09227" r="7.9375" />
        </g>
        <g transform="translate(-23.134525,-86.167262)">
          <text x="31.377192" y="97.486748" class="number-0"><tspan x="31.377192" y="97.486748">0</tspan></text>
          <text x="51.12534" y="97.490875" class="number-1"><tspan x="51.12534" y="97.490875">1</tspan></text>
          <text x="71.204216" y="97.546692" class="number-2"><tspan x="71.204216" y="97.546692">2</tspan></text>
          <text x="90.926529" y="97.486748" class="number-4"><tspan x="90.926529" y="97.486748">4</tspan></text>
          <text x="110.78836" y="97.490875" class="number-4"><tspan x="110.78836" y="97.490875">4</tspan></text>
          <text x="31.428869" y="117.27468" class="number-5"><tspan x="31.428869" y="117.27468">5</tspan></text>
          <text x="51.182186" y="117.3305" class="number-6"><tspan x="51.182186" y="117.3305">6</tspan></text>
          <text x="71.080193" y="117.33463" class="number-7"><tspan x="71.080193" y="117.33463">7</tspan></text>
          <text x="90.90844" y="117.3305" class="number-8"><tspan x="90.90844" y="117.3305">8</tspan></text>
          <text x="110.78836" y="117.3305" class="number-9"><tspan x="110.78836" y="117.3305">9</tspan></text>
          <text x="31.144648" y="137.17426" class="number-10"><tspan x="31.144648" y="137.17426">10</tspan></text>
          <text x="51.12534" y="137.17839" class="number-11"><tspan x="51.12534" y="137.17839">11</tspan></text>
          <text x="71.010429" y="137.23419" class="number-12"><tspan x="71.010429" y="137.23419">12</tspan></text>
          <text x="90.748245" y="137.17426" class="number-13"><tspan x="90.748245" y="137.17426">13</tspan></text>
          <text x="110.46538" y="137.17839" class="number-14"><tspan x="110.46538" y="137.17839">14</tspan></text>
        </g>
      </svg>
    </div>
  </div>

  <div id="granovetter-4" class="step" data-rel-x="0" data-rel-y="0">
    <div class="frame flex-h flex-align-center">
      <span class="effect"></span>
      <svg class="granovetter granovetter-animated granovetter-animated-cut3" viewBox="0 0 95.849989 56.162505" version="1.1">
        <g transform="translate(-23.134525,-86.167262)">
          <circle class="dot-0" cx="31.372025" cy="94.404762" r="7.9375" />
          <circle class="dot-1" cx="51.215775" cy="94.404762" r="7.9375" />
          <circle class="dot-2" cx="71.059525" cy="94.404762" r="7.9375" />
          <circle class="dot-cut" cx="90.903275" cy="94.404762" r="7.9375" />
          <circle class="dot-4" cx="110.74702" cy="94.404762" r="7.9375" />
          <circle class="dot-5" cx="31.372025" cy="114.24851" r="7.9375" />
          <circle class="dot-6" cx="51.215775" cy="114.24851" r="7.9375" />
          <circle class="dot-7" cx="71.059525" cy="114.24851" r="7.9375" />
          <circle class="dot-8" cx="90.903275" cy="114.24851" r="7.9375" />
          <circle class="dot-9" cx="110.74702" cy="114.24851" r="7.9375" />
          <circle class="dot-10" cx="31.372025" cy="134.09227" r="7.9375" />
          <circle class="dot-11" cx="51.215775" cy="134.09227" r="7.9375" />
          <circle class="dot-12" cx="71.059525" cy="134.09227" r="7.9375" />
          <circle class="dot-13" cx="90.903275" cy="134.09227" r="7.9375" />
          <circle class="dot-14" cx="110.74702" cy="134.09227" r="7.9375" />
        </g>
        <g transform="translate(-23.134525,-86.167262)">
          <text x="31.377192" y="97.486748" class="number-0"><tspan x="31.377192" y="97.486748">0</tspan></text>
          <text x="51.12534" y="97.490875" class="number-1"><tspan x="51.12534" y="97.490875">1</tspan></text>
          <text x="71.204216" y="97.546692" class="number-2"><tspan x="71.204216" y="97.546692">2</tspan></text>
          <text x="90.926529" y="97.486748" class="number-4"><tspan x="90.926529" y="97.486748">4</tspan></text>
          <text x="110.78836" y="97.490875" class="number-4"><tspan x="110.78836" y="97.490875">4</tspan></text>
          <text x="31.428869" y="117.27468" class="number-5"><tspan x="31.428869" y="117.27468">5</tspan></text>
          <text x="51.182186" y="117.3305" class="number-6"><tspan x="51.182186" y="117.3305">6</tspan></text>
          <text x="71.080193" y="117.33463" class="number-7"><tspan x="71.080193" y="117.33463">7</tspan></text>
          <text x="90.90844" y="117.3305" class="number-8"><tspan x="90.90844" y="117.3305">8</tspan></text>
          <text x="110.78836" y="117.3305" class="number-9"><tspan x="110.78836" y="117.3305">9</tspan></text>
          <text x="31.144648" y="137.17426" class="number-10"><tspan x="31.144648" y="137.17426">10</tspan></text>
          <text x="51.12534" y="137.17839" class="number-11"><tspan x="51.12534" y="137.17839">11</tspan></text>
          <text x="71.010429" y="137.23419" class="number-12"><tspan x="71.010429" y="137.23419">12</tspan></text>
          <text x="90.748245" y="137.17426" class="number-13"><tspan x="90.748245" y="137.17426">13</tspan></text>
          <text x="110.46538" y="137.17839" class="number-14"><tspan x="110.46538" y="137.17839">14</tspan></text>
        </g>
      </svg>
    </div>
  </div>

  <div id="information-cascade" class="step" data-rel-x="1024">
    <h2 class="supertitle">Inspirations</h2>
    <h1>Network spreading processes</h1>
    <p><a class="cite" href="http://www.pnas.org/content/99/9/5766">Watts 2002</a></p>
    <div id="elm"></div>
  </div>

  <div id="simulations" class="step" data-x="-512" data-rel-y="1024">
    <h2 class="supertitle">Projects</h2>
    <h1>Simulations</h1>
    <div class="flex-h flex-align-center flex-justify-even">
      <img width="250px" src="img/simulation-network-seed.svg" />
      <span class="effect">?</span>
      <img width="250px" src="img/simulation-network-spread.svg" />
    </div>
    <p>When competing, how many nodes/edges<br />are necessary for global adoption?</p>
    <div class="flex-h flex-justify-between">
      <p>What are the effects of</p>
      <ul class="left-line">
        <li>node diversity</li>
        <li>network topology</li>
        <li>placement of seed nodes or constrained edges</li>
      </ul>
    </div>
  </div>

  <div id="experiments" class="step" data-rel-x="1024" data-rel-y="0">
    <h2 class="supertitle">Projects</h2>
    <h1>Experiments</h1>
    <div class="flex-h flex-align-center flex-justify-even">
      <ul class="right-line align-right">
        <li>One-way communication<br /><span class="minor">~ Iterated Learning</span></li>
        <li>Interactive communication<br ?><span class="minor">~ Experimental Semiotics</span></li>
        <li>Minimal interactive coupling<br ?><span class="minor">~ Perceptual crossing</span></li>
      </ul>
      <p>playing with</p>
      <ul class="left-line">
        <li>Network topology</li>
        <li>Edge constraints</li>
        <li>Individual sensitivites</li>
      </ul>
    </div>
    <div class="flex-h flex-align-center flex-justify-between">
      <div>
        <img width="250px" src="img/de-jaegher-et-al-2010.png" />
        <p><a class="cite" href="http://www.cell.com/article/S1364661310001464/abstract">De Jaegher <em>et al.</em> 2010</a> <span class="licence">[CC-BY]</span></p>
      </div>
      <div>
        <img width="250px" src="img/exp-network.svg" />
        <p><span class="licence">Icons by Font Awesome [CC-BY]</span></p>
      </div>
      <p class="align-left"><span class="minor">Possibly Twitter<br />25% GMT+0/1<br />tweets in French</span></p>
    </div>
  </div>

  <div id="summary" class="step" data-x="0" data-y="1536" data-rel-x="0" data-rel-y="0" data-scale="6">
    <ul>
      <li><a href="https://slvh.fr">slvh.fr</a></li>
      <li><a href="http://twitter.com/wehlutyk">@wehlutyk</a></li>
    </ul>
    <p class="align-right">Thank you!</p>
  </div>

</div>

<div id="impress-toolbar"></div>

<script src="js/impress.min.js"></script>
<script>impress().init();</script>
<script src="flush/dist/d3-dispatch.min.js"></script>
<script src="flush/dist/d3-quadtree.min.js"></script>
<script src="flush/dist/d3-timer.min.js"></script>
<script src="flush/dist/d3-force.min.js"></script>
<script src="flush/dist/flush.js"></script>
<script src="flush/dist/index.js"></script>

</body>
</html>
