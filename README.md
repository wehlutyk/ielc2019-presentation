IELC 2019 Presentation
======================

Html source of the slides for [Dediu, Karsai, Lerique & Magué, "Individual variation, network heterogeneity and linguistic complexity: which way does the relationship go?"](http://www.lel.ed.ac.uk/~kenny/ielc2019Abstracts/Dediu.pdf), presented at [IELC 2019](http://www.lel.ed.ac.uk/cle/index.php/ielc2019/) (using [Impress.js](https://impress.js.org/)).

To make changes: clone locally, then `npm install`, then `npm run serve`, and watch  [`localhost:8080`](http://localhost:8080/) open up in your browser.
